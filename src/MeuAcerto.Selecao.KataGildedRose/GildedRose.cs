﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            foreach (var item in Itens)
            {
                if (!item.Nome.Equals("Dente do Tarrasque"))
                {
                    item.PrazoParaVenda -= 1;

                    if (item.Nome.Equals("Queijo Brie Envelhecido"))
                    {
                        if (item.PrazoParaVenda < 0)
                            item.Qualidade += 2;
                        else
                            item.Qualidade += 1;
                    }
                    else if (item.Nome.Equals("Ingressos para o concerto do Turisas"))
                    {
                        if (item.PrazoParaVenda < 0)
                            item.Qualidade = 0;
                        else if (item.Qualidade < 50)
                        {
                            if (item.PrazoParaVenda <= 5)
                                item.Qualidade += 3;
                            else if (item.PrazoParaVenda <= 10)
                                item.Qualidade += 2;
                            else
                                item.Qualidade += 1;
                        }
                    }
                    else if (item.Nome.Equals("Bolo de Mana Conjurado"))
                    {
                        if (item.PrazoParaVenda < 0)
                            item.Qualidade -= 4;
                        else
                            item.Qualidade -= 2;
                    }
                    else
                    {
                        if (item.PrazoParaVenda < 0)
                            item.Qualidade -= 2;
                        else
                            item.Qualidade -= 1;
                    }

                    if (item.Qualidade > 50)
                        item.Qualidade = 50;
                    else if (item.Qualidade < 0)
                        item.Qualidade = 0;
                }
            }
        }
    }
}
