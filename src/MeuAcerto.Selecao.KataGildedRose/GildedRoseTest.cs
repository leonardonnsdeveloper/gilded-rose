﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseTest
    {
        [Fact]
        public void AtualizarQualidade_DenteDoTarrasque()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Dente do Tarrasque", PrazoParaVenda = 40, Qualidade = 40 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(40, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_QueijoBrieEnvelhecido()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Queijo Brie Envelhecido", PrazoParaVenda = 8, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(5, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_QueijoBrieEnvelhecido_PrazoPassado()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Queijo Brie Envelhecido", PrazoParaVenda = 0, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(6, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_BoloDeManaConjurado()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoParaVenda = 8, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(2, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_BoloDeManaConjurado_PrazoPassado()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoParaVenda = 0, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(0, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_IngressosParaOConcertoDoTurisas()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do Turisas", PrazoParaVenda = 12, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(5, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_IngressosParaOConcertoDoTurisas_PrazoPassado()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do Turisas", PrazoParaVenda = 0, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(0, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_IngressosParaOConcertoDoTurisas_PrazoMenorIgualDez()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do Turisas", PrazoParaVenda = 11, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(6, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_IngressosParaOConcertoDoTurisas_PrazoMenorIgualCinco()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do Turisas", PrazoParaVenda = 6, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(7, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ElixirDoMangusto()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Elixir do Mangusto", PrazoParaVenda = 6, Qualidade = 4 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(3, Items[0].Qualidade);
        }
    }
}
